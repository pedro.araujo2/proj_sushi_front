import React from 'react'
import { Container } from 'react-bootstrap'
import styled from 'styled-components'


export default ( {title , sub } ) => {
    return (
        <ContainerTitle>
            <Container>
            <Title>
                { title }
            </Title>
            <Sub>
                { sub }
            </Sub>
            </Container>                        
        </ContainerTitle>
    )
}

const ContainerTitle = styled.div`
    background: #BD2132;
    padding: 10px;
    border-bottom: 1px solid #BD2132;
    font-family: 'Poppins', sans-serif;
`

const Title = styled.div`
    color: #fff;
    font-size: 28px;
    font-weight: 600;

`
const Sub = styled.div`
    color: #eee;
    font-family: 'Josefin Sans', sans-serif;
`
