import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import styled from 'styled-components'
import { AiFillFacebook } from 'react-icons/ai'
import { FiInstagram } from 'react-icons/fi'

export default () => {
    return (
        <Footer>
            <Container>
                <FooterInfo>
                    <Row>
                        <Col md={5}>
                            <div className="title">Sobre Nós</div>
                            <div className="aboutUs">
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident.</p>
                            </div>
                        </Col>
                        <Col md={4}>
                            <div className="title">Nossos Destaques</div>
                            <div className="menu">
                                <div className="text">
                                    <div><span className="icon-calendar"></span>Sept 15, 2018</div>
                                    <div><span className="icon-person"></span> Admin</div>
                                    <div><span className="icon-chat"></span> 19</div>
                                </div>
                            </div>
                        </Col>
                        <Col md={3}>
                            <div className="title">Onde estamos</div>
                            <div className="address">
                                <div className="tblock-23 mb-3">
                                    <ul>
                                        <li><span className="icon-calendar"></span>Sept 15, 2018</li>
                                        <li><span className="icon-person"></span> Admin</li>
                                        <li><span className="icon-chat"></span> 19</li>
                                    </ul>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </FooterInfo>
                <Row>
                    <FooterSocial>
                        <AiFillFacebook />
                        <FiInstagram />
                    </FooterSocial>

                    <FooterCopy>
                        Todos os direitos reservados.
                </FooterCopy>
                </Row>
            </Container>
        </Footer>
    )
}

const Footer = styled.div`
    background: #090B0D;
    padding: 10px 0;
    color: #eee;
    border-top: 2px solid #BD2132;
    font-weight: 300;   
`
const FooterInfo = styled.div`    
    .title{
        font-size: 20px;
        font-weight: 600px;
        padding: 10px 0;
        border-bottom: thin solid #BD2132;
        margin-bottom: 10px;
        color: white;
        font-weight: 700;                
    }
`

const FooterSocial = styled.div`
    width: 100%;
    border-bottom: 1px dotted #ccc;
    padding: 10px;
    
    svg {
        cursor: pointer;
        margin: 5px;
        font-size: 30px;
        :hover{
            color:#BD2132;
        }
    }    
`

const FooterCopy = styled.div`
    width: 100%;
    padding: 10px;
    text-align: center;    
`