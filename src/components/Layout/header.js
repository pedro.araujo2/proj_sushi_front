import React from 'react'
import { Nav, Navbar, Container } from 'react-bootstrap'
import styled from 'styled-components'
import imgLogo from '../../assets/images/logo-proj_sushi.png'
import { NavLink } from 'react-router-dom'



export default () => {

    const menu = [
        {
            title: "Home",
            link: '',
            icon: ''
        },
        {
            title: "Sobre",
            link: 'sobre',
            icon: ''
        },
        {
            title: "Produtos",
            link: 'produtos',
            icon: ''
        },
        {
            title: "Serviços",
            link: 'servicos',
            icon: ''
        },
        {
            title: "Contato",
            link: 'contato',
            icon: ''
        }
    ]

    return (
        <Header>
            <Container>
                <Navbar expand="lg" variant="dark">
                    <Navbar.Brand href="#home">
                        <Logo>
                            <img src={imgLogo} className="logo" alt="Logo Oishī Sushi" />
                            <span>Oishī Sushi</span>
                        </Logo>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse className="justify-content-end">
                        <Nav>
                            {menu.map((item, i) => (
                                <NavLink exact={true} to={item.link} key={i} >
                                    <Nav.Link as="div">{item.title}</Nav.Link >
                                </NavLink>
                            ))}
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </Container>
        </Header>
    )
}

const Header = styled.div`
    background-color: #090B0D; 
    font-family: 'Poppins', sans-serif;  
    color: #eee;
    a {
        color: #eee;
        text-decoration: none;
        font-weight: 500;
    }

    .nav-link {
        
        :hover {
            background-color: #BD2132;
            color: #eee;
            font-weight: 600;
            text-decoration: none;
        }            
    }
`

const Logo = styled.div`
    text-align: center;     

    img {
    width: 40px;
    margin-bottom: -4px;    
    }

    span {
        display:block;               
        text-align: center;        
        font-size: 26px;
        font-weight: 480;
        color: #eee;        
        font-family: 'Shadows Into Light', cursive;    
    }
`