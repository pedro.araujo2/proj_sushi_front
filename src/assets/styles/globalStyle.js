import { createGlobalStyle } from 'styled-components'
import 'bootstrap/dist/css/bootstrap.min.css';
import ImgBg from '../images/bg_black_1.jpg'

const GlobalStyle = createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        outline: 0;
        -webkit-font-smoothing: antialiased;
        
    }

    body {
        background: url(${ImgBg}) no-repeat fixed;
        background-size: cover;
    }
`
export default GlobalStyle;