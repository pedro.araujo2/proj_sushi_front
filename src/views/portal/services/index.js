import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import styled from 'styled-components'
import TitlePage from '../../../components/titlePage'

export default () => {
    return (        
        <Service> 
            <TitlePage title="Serviços" sub="Conheça nossos serviços" />

            <Container>
                <Row>
                    <ServiceItem>First, but unordered</ServiceItem>
                    <ServiceItem>Second, but last</ServiceItem>
                    <ServiceItem>Second, but last</ServiceItem>
                </Row>
            </Container>
        </Service>       
    )
}

const Service = styled.div`
    display: block;
    height: 500px;
`

const ServiceItem = styled(Col)`
    background: red;
    height: 200px;
    margin: 10px;
    width: 20%;
`