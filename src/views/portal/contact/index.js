import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import styled from 'styled-components'
import TitlePage from '../../../components/titlePage'


export default () => {
    return (        
        <Contact>
            <TitlePage title="Contato" sub="Nossos Contatos" />

            <Container>
                <Row>
                    <InfoItem md={4}></InfoItem>
                    <FormItem md={8}></FormItem>
                </Row>
            </Container>

        </Contact>       
    )
}

const Contact = styled.div`
    display: block;
    height: 500px;
`
const InfoItem = styled(Col)`
    background: red;
    height: 300px;    
    width: 100%;
    
`
const FormItem = styled(Col)`
    background: blue;
    height: 300px;    
    width: 1000%;
    
`