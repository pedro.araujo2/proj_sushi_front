import React from 'react'
//import { Alert } from 'react-bootstrap'
import styled from 'styled-components'
//import { BiHome } from 'react-icons/bi'

import Banner from './banner'
import About from './about'
import Menu from './menu'
import Products from './products'
import Info from './info'


const Home = () => {
    return (
        <HomeContainer>
            <Banner />
            <About />
            <Info />
            <Menu />
            <Products /> 
        </HomeContainer>
    )
}

export default Home

const HomeContainer = styled.div`
    
    
`
