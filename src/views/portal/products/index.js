import React from 'react'
import { Tab, Tabs } from 'react-bootstrap'
import styled from 'styled-components'
import TitlePage from '../../../components/titlePage'

export default () => {
    return (        
        <Products> 
            <TitlePage title="Produtos" sub="Conheça nossa lista de produtos" />
            
            <TabBox defaultActiveKey="profile" id="uncontrolled-tab-example">
                <Tab eventKey="home" title="Home">
                O! from what power hast thou this powerful might, With insufficiency my heart to sway? To make me give the lie to my true sight, And swear that brightness doth not grace the day? Whence hast thou this becoming of things ill, That in the very refuse of thy deeds There is such strength and warrantise of skill, That, in my mind, thy worst all best exceeds? Who taught thee how to make me love thee more, The more I hear and see just cause of hate?
                </Tab>
                <Tab eventKey="profile" title="Profile">
                O! from what power hast thou this powerful might, hast thou this becoming of things ill, That in the very refuse of thy deeds There is such strength and warrantise of skill, That, in my mind, thy worst all best exceeds? Who taught thee how to make me love thee more, The more I hear and see just cause of hate?
                </Tab>
                <Tab eventKey="contact" title="Contact">
                O! from what power hast thou this powerful might, With insufficiency my heart to sway? To make me give the lie to my true sight.
                </Tab>
            </TabBox>
        </Products>
    )
}

const Products = styled.div`
    display: block;
    height: 500px;
    
    .tab-content{
        background: #eee;
    }
`
const TabBox = styled(Tabs)`
    background: #fff;
    
`